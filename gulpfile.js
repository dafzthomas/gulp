const gulp = require('gulp');
const util = require('gulp-util');
const clip = require('gulp-clip-empty-files');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const typescript = require('gulp-typescript');

const is_production = util.env.type === 'production' ? true : false;

const opts = {
	scss: {
		source: './source/scss/**/*.scss',
		dest: './dist/css'
	},
	ts: {
		source: './source/scripts/**/*.ts',
		dest: './dist/scripts'
	}
}

gulp.task('default', () => {
	util.log('Gulp is running!');
	util.log(is_production ? 'Running production mode...' : 'Running non production mode...');
	gulp.watch(opts.scss.source, ['sass']);
	gulp.watch(opts.ts.source, ['ts']);
});

gulp.task('sass', () => {
	gulp.src(opts.scss.source)
		.pipe(clip())
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(!is_production ? sourcemaps.init() : util.noop())
		.pipe(sass(is_production ? {
			outputStyle: 'compressed'
		} : {
			outputStyle: 'expanded',
			indentWidth: 4
		}).on('error', sass.logError))
		.pipe(!is_production ? sourcemaps.write('./maps') : util.noop())
		.pipe(gulp.dest(opts.scss.dest));
});

gulp.task('ts', () => {
	gulp.src(opts.ts.source)
		.pipe(clip())
		.pipe(!is_production ? sourcemaps.init() : util.noop())
		.pipe(typescript({
			noImplicitAny: true,
			allowJs: true,
			removeComments: true,
			target: 'ES5',
			out: 'build.js',
		}))
		.pipe(!is_production ? sourcemaps.write('./maps') : util.noop())
		.pipe(gulp.dest(opts.ts.dest));
});